import { Component, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';

import { AlertComponent } from './dialog/alert/alert.component';
import { DialogComponent } from './dialog/confirm/dialog.component';
import { ClienteService } from './services/cliente.service';
import { UploadImgService } from './services/upload-img.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {

  @Input() cod_cliente!: string;
  @Input() nome_loja!: string;
  @Input() cnpj!: string;
  @Input() ano_ini!: string;
  @Input() solicita_motivo_limite: boolean = false;
  @Input() show_alert: boolean = false;
  @Input() maisDeUmaLoja: string = 'não';
  @Input() maisDeUmCanal: string = 'não';

  @Input() faturamento: number = 0;
  @Input() limite: number = 0;
  @Input() limite_verificado: number = 0;

  selectedValue1: number = 0;
  selectedValue2: number = 0;
  numbers1: number[] = [0];
  numbers2: number[] = [0];

  constructor(
    public dialog: MatDialog,
    private _router: Router,
    private _cliente: ClienteService,
    private _upload: UploadImgService
  ) {}

  clientes = new FormGroup({
    cod_cli: new FormControl('', Validators.required),
    cod_grupo: new FormControl('', Validators.required),
    nome_loja: new FormControl('', Validators.required),
    ano_inauguracao: new FormControl('', Validators.required),
    cnpj: new FormControl(),
    faturamento_mes: new FormControl('', Validators.required),
    limite: new FormControl('', Validators.required),
    qnt_marcas: new FormControl(1, Validators.required),
    nome_marcas1: new FormControl('Labellamafia', Validators.required),
    tempo_com_marca1: new FormControl(),
    qnt_percentual1: new FormControl(100, Validators.required),
    nome_marcas2: new FormControl(),
    tempo_com_marca2: new FormControl(),
    qnt_percentual2: new FormControl(),
    nome_marcas3: new FormControl(),
    tempo_com_marca3: new FormControl(),
    qnt_percentual3: new FormControl(),
    nome_marcas4: new FormControl(),
    tempo_com_marca4: new FormControl(),
    qnt_percentual4: new FormControl(),
    nome_marcas5: new FormControl(),
    tempo_com_marca5: new FormControl(),
    qnt_percentual5: new FormControl(),
    nome_marcas6: new FormControl(),
    tempo_com_marca6: new FormControl(),
    qnt_percentual6: new FormControl(),
    share: new FormControl('', Validators.required),
    limite_exedido: new FormControl(),
    solicita_aumento: new FormControl('', Validators.required),
    motivo: new FormControl('', Validators.required),
    aceita_avaliar_pedido: new FormControl('sim', Validators.required),
    possui_mais_de_uma_loja: new FormControl('', Validators.required),
    qnt_lojas: new FormControl(),
    local1: new FormControl(),
    nomes_lojas1: new FormControl(),
    local2: new FormControl(),
    nomes_lojas2: new FormControl(),
    local3: new FormControl(),
    nomes_lojas3: new FormControl(),
    local4: new FormControl(),
    nomes_lojas4: new FormControl(),
    local5: new FormControl(),
    nomes_lojas5: new FormControl(),
    local6: new FormControl(),
    nomes_lojas6: new FormControl(),
    local7: new FormControl(),
    nomes_lojas7: new FormControl(),
    local8: new FormControl(),
    nomes_lojas8: new FormControl(),
    possui_canal_de_venda: new FormControl('', Validators.required),
    canais_digitais: new FormControl(),
    canal_venda_interessados: new FormControl(),
    canal_venda_mais_vendas: new FormControl(),
    canal_vendas_dificuldade: new FormControl(),
    endereco_canal_ecommerce: new FormControl(),
    endereco_canal_instagram: new FormControl(),
    endereco_canal_facebook: new FormControl(),
    endereco_canal_whatsapp: new FormControl(),
    qnt_vendedores: new FormControl('', Validators.required),
    vitrine_expositor: new FormControl('', Validators.required),
    tamanho_loja: new FormControl('', Validators.required),
    qual_publico: new FormControl('', Validators.required),
    mix_ofertado: new FormControl('', Validators.required),
    percentual_casual: new FormControl('', Validators.required),
    percentual_fitness: new FormControl('', Validators.required),
    percentual_acessorios: new FormControl('', Validators.required),
    percentual_calcados: new FormControl('', Validators.required),
    percentual_beachwear: new FormControl('', Validators.required),
    considera_essencial: new FormControl('', Validators.required),
    razoes_vender_muito: new FormControl('', Validators.required),
    momento_maior_vendas: new FormControl('', Validators.required),
    essencial_compra_colecao: new FormControl('', Validators.required),
    marketing_sente_falta: new FormControl('', Validators.required),
    marketing_ajuda_muito: new FormControl('', Validators.required),
    // email: new FormControl('', Validators.required),
    // isntagram_pessoal: new FormControl('', Validators.required),
    // isntagram_loja: new FormControl('', Validators.required),
  });

  getClientes() {
    this._cliente.getCliente(this.cod_cliente).subscribe({
      next: (res) => {
        this.nome_loja = res.nome_fantasia_f;
        this.cnpj = res.cnpj_f;

        this.clientes.patchValue({
          nome_loja: res.nome_fantasia_f,
          cnpj: res.cnpj_f,
        });
        this.show_alert = false;
      },
      error: (err) => {
        console.log('cod não encontrado', err);
        this.show_alert = true;
      },
    });
  }

  verificaFaturamento() {
    this.limite_verificado =
      this.faturamento * (this.clientes.get('qnt_percentual1')!.value / 100);
    this.clientes.patchValue({ share: this.limite_verificado });
    this.solicita_motivo_limite = false
    if (this.limite < this.limite_verificado){
      this.solicita_motivo_limite = true
    }
  }

  getQntMarcas() {
    for (var i = 0; i < this.selectedValue1; i++) {
      this.numbers1[i] = i;
    }
  }
  getQntLojas() {
    for (var i = 0; i < this.selectedValue2; i++) {
      this.numbers2[i] = i;
    }
  }

  uploadImg(event: any) {
    if (event.target.files && event.target.files[0]) {
      const filename = event.target.files[0];
      const ext = filename.name.split('.')[1];

      const ALLOWED_EXTENSIONS = ['pdf', 'png', 'jpg', 'jpeg'];

      if (ALLOWED_EXTENSIONS.includes(ext.toLowerCase())) {
        const formData = new FormData();
        formData.append('arq', filename);

        this._upload
          .setUpload(formData)
          .subscribe({
            next: (n) => {
              console.log('Arquivo enviado!');
            },
            error: (e) => {
              console.error(e)
            }
          });
      } else {
        this.dialog.open(AlertComponent);
      }
    }
  }

  salvar() {
    const dialogRef = this.dialog.open(DialogComponent, {
      data: {
        message: 'Salvar formulário?',
        buttonText: {
          ok: 'Sim',
          cancel: 'Cancelar',
        },
      },
    });
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      if (confirmed) {
        this._cliente.setCliente(this.clientes.value).subscribe({
          next: (v) => {
            this._cliente.message('Formulário salvo com sucesso!');
            window.location.reload();
          },
          error: (e) => {
            console.error(e);
            this._cliente.message('Erro ao enviar o formulário!');
          },
        });
      }
    });
  }

  cancelar() {
    this.clientes.reset();
    this.clientes.patchValue({
      nome_marcas1: 'Labellamafia',
      qnt_percentual1: 100,
    });
  }
}
