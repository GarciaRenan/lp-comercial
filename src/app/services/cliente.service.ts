import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

import { Cliente } from '../models/cliente';

const API = environment.API_ENV;

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  constructor(private _httpClient: HttpClient, private _snackBar: MatSnackBar) { }

  getCliente(id: string): Observable<Cliente> {
    return this._httpClient.get<Cliente>(`${API}/api/cliente/${id}`);
  }

  setCliente(cliente: any): Observable<any> {
    const body = JSON.stringify(cliente)
    return this._httpClient.post<any>(`${API}/api/setcliente`, body);
  }


  message(msg: string): void {
    this._snackBar.open(msg, 'X', {
      duration: 3000,
      horizontalPosition: "end",
      verticalPosition: "bottom"
    })
  }
}
